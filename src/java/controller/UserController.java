/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author SandyRizqi
 */
@WebServlet(urlPatterns = {"/usercontroller"})
public class UserController extends HttpServlet {

    private void userAdd(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.getWriter().write(request.getParameter("namalengkap"));
    }

    private void userEdit(HttpServletRequest request, HttpServletResponse response, String teString) throws IOException {
        response.getWriter().write(Page.baseUrl);
        response.getWriter().write(request.getParameter("namalengkap"));
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter("action")) {
            case "userAdd":
                userAdd(request, response);
                break;
            case "userEdit":
                userEdit(request, response, "Ini edit budi");
                break;
        }

        // Add Session
//        request.getSession(true).setAttribute("key", "value");
        // Get Session
//        request.getSession().getAttribute("")
        // Remove Session
//        request.getSession().removeAttribute("key");
        // Clear Session
//        request.getSession().invalidate();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
