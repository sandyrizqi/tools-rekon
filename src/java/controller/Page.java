/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author SandyRizqi
 */
@WebServlet(urlPatterns = "/page/*")
public class Page extends HttpServlet {

    public static String baseUrl = "";
    private String driver = "oracle.jdbc.driver.OracleDriver";
    private String koneksi = "jdbc:oracle:thin:@localhost:1521:orcl";
    private String user = "rekon";
    private String pass = "Telkomsigma123";
    public Connection con;
    public Statement st;
    public ResultSet rs;
    // public String tampil="";

    public void koneksifunction() throws ClassNotFoundException {

        String hasil = "";
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(koneksi, user, pass);
            hasil = "Koneksi oke";
        } catch (SQLException e) {
            hasil = "Gagal Konek dengan error" + e;
        }

        //return hasil;
    }

    public boolean getdatauser() throws Exception {
        this.koneksifunction();
        st = con.createStatement();
        rs = st.executeQuery("select * from USERMGMT");
        return rs.next();
    }

    public boolean buatstatement() throws Exception {
        st = con.createStatement();
        rs = st.executeQuery("select * from USERMGMT");
        return (rs != null);
    }

    public boolean buatnext() throws Exception {
        return rs.next();
    }

    public List<Map<String, String>> tampil() {
        List<Map<String, String>> list = new ArrayList<>();

        try {
            PreparedStatement statement = prepare("SELECT * FROM USERMGMT");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Map<String, String> map = new TreeMap<>();
                map.put("id", resultSet.getString("ID"));
                map.put("nama", resultSet.getString("NAMA"));
                map.put("email", resultSet.getString("EMAIL"));
                map.put("nik", resultSet.getString("NIK"));
                
                list.add(map);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException ex) {
        }

        return list;
    }

    private PreparedStatement prepare(String query) throws SQLException {
        DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
        return DriverManager.getConnection(koneksi, user, pass).prepareStatement(query);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException {
        if (baseUrl.isEmpty()) {
            baseUrl = "http://" + request.getServerName() + ":8080" + request.getContextPath() + "/";
        }
        request.setAttribute("baseUrl", baseUrl);
//        String koneksi=this.koneksifunction();
//        request.setAttribute("koneksi", koneksi);

        String page = "/WEB-INF/page/";
        String segment = request.getPathInfo();

        if (segment == null) {
            // Berarti cuma /page
            page += "/dashboard";
        } else {
            switch (request.getPathInfo()) {
                case "/dashboard":
                    page += "dashboard";
                    break;
                case "/useradd":
                    page += "useradd";
                    break;
                case "/login":
                    request.getSession(true).setAttribute("nama", "test");
                    page += "login";
                    break;
                case "/rekon":
                    page += "/formrekon";
                    break;
                case "/report":
                    page += "/report";
                    break;
                case "/usermanagement":
                    request.setAttribute("datauser", tampil());
                    page += "/usermgt";
                    break;
            }
        }

        page += ".jsp";
        request.getRequestDispatcher(page).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Page.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Page.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
