
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>

<div class="wrapper">
    <%@include file="sidebar.jsp" %>
    <%@include file="navbar.jsp" %>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Form Tool Rekon</h4>
                                <p class="category"></p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>No</th>
                                    	<th>Serial Number</th>
                                    	<th>Nomor Layanan</th>
                                    	<th>Mac Address</th>
                                    	<th>Status ONT (API)</th>
                                        <th>Status ONT (SCMT)</th>
                                        <th></th>


                                    </thead>
                                    <tbody>
                                        <% for (int i = 1; i <= 10; i++) {

%>
                                        <tr>
                                            <td><%=i%></td>
                                                <td>110101010</td>
                                        	<td>12345678</td>
                                        	<td>0000.0000.0000</td>
                                        	<td>ON</td>
                                        	<td>ON</td>
                                                <td><input type="checkbox"></td>
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


     <%@include file="footer.jsp" %>
