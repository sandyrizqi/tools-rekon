
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>


<body>

    <div class="wrapper">
        <%@include file="sidebar.jsp" %>
        <%@include file="navbar.jsp" %>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Add User </h4>
                            </div>
                            <div class="content">
                                <form action="${baseUrl}usercontroller" method="post">
                                    <input name="action" type="hidden" value="userEdit"/>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama Lengkap</label>
                                                <input name="namalengkap" type="text" class="form-control" placeholder="Nama" value="">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">NIK</label>
                                                <input name="nik" type="text" class="form-control" placeholder="Nik">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" class="form-control" placeholder="Email" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Level</label>
                                                <select class="form-control">
                                                    <option>--Silahkan Pilih Level--</option>
                                                    <option>Administrator</option>
                                                    <option>User</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" placeholder=Password" value="">
                                            </div>
                                        </div>
                                    </div>




                                    <button type="submit" class="btn btn-info btn-fill pull-right">Submit</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--                    <div class="col-md-4">
                                            <div class="card card-user">
                                                <div class="image">
                                                    <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>
                                                </div>
                                                <div class="content">
                                                    <div class="author">
                                                         <a href="#">
                                                        <img class="avatar border-gray" src="assets/img/faces/face-3.jpg" alt="..."/>
                    
                                                          <h4 class="title">Mike Andrew<br />
                                                             <small>michael24</small>
                                                          </h4>
                                                        </a>
                                                    </div>
                                                    <p class="description text-center"> "Lamborghini Mercy <br>
                                                                        Your chick she so thirsty <br>
                                                                        I'm in that two seat Lambo"
                                                    </p>
                                                </div>
                                                <hr>
                                                <div class="text-center">
                                                    <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                                                    <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                                                    <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>
                    
                                                </div>
                                            </div>
                                        </div>-->

                </div>
            </div>
        </div>


        <%@include file="footer.jsp" %>
