
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>

<div class="wrapper">
    <%@include file="sidebar.jsp" %>
    <%@include file="navbar.jsp" %>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Report Tool Rekon</h4>
                                <p class="category"></p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Start Date</label>
                                                <input type="date" class="form-control" placeholder="Company" value="Creative Code Inc.">
                                            </div>
                                        </div>
                                    <div class="col-md-4">
                                            <div class="form-group">
                                                <label>End Date</label>
                                                <input type="date" class="form-control" placeholder="Company" value="Creative Code Inc.">
                                            </div>
                                        </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>&nbsp;<br></label>
                                               <input type="submit" class="btn btn-primary" value="Submit" style="margin-top: 1.8em;">
                                            </div>
                                        
                                    </div>
                                    
                                </div>
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>No</th>
                                    	<th>Serial Number</th>
                                    	<th>Nomor Layanan</th>
                                    	<th>Mac Address</th>
                                    	<th>Installed By</th>
                                        <th>Date</th>
                                        


                                    </thead>
                                    <tbody>
                                        <% for (int i = 1; i <= 10; i++) {

%>
                                        <tr>
                                            <td><%=i%></td>
                                                <td>110101010</td>
                                        	<td>12345678</td>
                                        	<td>0000.0000.0000</td>
                                        	<td>Normal</td>
                                        	<td>23 Juli 2017</td>
                                                
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


     <%@include file="footer.jsp" %>
