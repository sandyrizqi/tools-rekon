<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<jsp:useBean id="o" class="controller.Page" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>

    <div class="wrapper">
        <%@include file="sidebar.jsp" %>
        <%@include file="navbar.jsp" %>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">User Management</h4>
                                <a href="${baseUrl}page/useradd" class="btn btn-warning btn-fill" style="float: right;"><i class="pe-7s-plus"></i> Add User</a>
                                <p class="category"></p>
                            </div>

                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <th>No</th>
                                    <th>Nama Lengkap</th>
                                    <th>Email</th>
                                    <th>NIK</th>
                                    <th>Level</th>
                                    <th>Last Login</th>
                                    <th>Aksi</th>



                                    </thead>
                                    <tbody>
                                        <%
                                            for (Map<String, String> each : (List<Map<String, String>>) request.getAttribute("datauser")) {
                                        %>
                                        <tr>
                                            <td><%="1"%></td>
                                            <td><%= each.get("nama") %></td>
                                            <td><%= each.get("email") %></td>
                                            <td><%= each.get("nik") %></td>
                                            <td>23 Juli 2017</td>
                                            <td>
                                                <a href="#" class="btn btn-danger"><i class="pe-7s-trash"></i></a>
                                                &nbsp;&nbsp;
                                                <a href="#" class="btn btn-primary"><i class="pe-7s-pen"></i></a>
                                            </td>
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <%@include file="footer.jsp" %>
