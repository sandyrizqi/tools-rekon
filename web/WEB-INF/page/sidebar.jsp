

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% String base_url="http://localhost:8080/jspnative/";%>
<div class="sidebar" data-color="red" data-image="${baseUrl}assets/img/logotelkom.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    RE Tools
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="${baseUrl}page/dashboard" class="tree-toggle">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                    <ul class="nav nav-list tree">
                                <li><a href="#"><i class="pe-7s-photo-gallery"></i>Sub Menu 1</a></li>
                                <li><a href="#"><i class="pe-7s-photo-gallery"></i>Sub Menu 2</a></li>
                            </ul>
                </li>
                <li>
                    <a href="${baseUrl}page/rekon">
                        <i class="pe-7s-network"></i>
                        <p>Rekon</p>
                    </a>
                </li>
                
                <li>
                    <a href="${baseUrl}page/report">
                        <i class="pe-7s-note2"></i>
                        <p>Report</p>
                    </a>
                </li>
                <li>
                    <a href="${baseUrl}page/usermanagement">
                        <i class="pe-7s-user"></i>
                        <p>User Management</p>
                    </a>
                </li>
                
				<!-- <li class="active-pro">
                    <a href="upgrade.html">
                        <i class="pe-7s-rocket"></i>
                        <p>Upgrade to PRO</p>
                    </a>
                </li> -->
            </ul>
    	</div>
    </div>
