
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>

            </ul>
        </nav>
        <p class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script> Telkom Indonesia
        </p>
    </div>
</footer>

</div>
</div>


</body>

<!--   Core JS Files   -->
<script src="${baseUrl}assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="${baseUrl}assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="${baseUrl}assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="${baseUrl}assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="${baseUrl}assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxhjOV3u2tjCB2gBI4hyi3FXO93f46PF8"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="${baseUrl}assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="${baseUrl}assets/js/demo.js"></script>

<script type="text/javascript">
                $(document).ready(function () {

                    demo.initChartist();

                    $.notify({
                        icon: 'pe-7s-gift',
                        message: "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

                    }, {
                        type: 'info',
                        timer: 4000
                    });

                    $('.tree-toggle').click(function () {
                        $(this).parent().children('ul.tree').toggle(200);
                    });
                    $(function () {
                        $('.tree-toggle').parent().children('ul.tree').toggle(200);
                    })

                });
</script>

</html>
